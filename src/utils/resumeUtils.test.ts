import getResumeText from './resumeUtils';

describe('resumeUtils', () => {
  test('should return resume markdown as HTML', async () => {
    window.fetch = jest.fn().mockResolvedValue({
      text: jest.fn().mockResolvedValue('SOME MARKDOWN'),
    });

    expect(await getResumeText()).toBe('<p>SOME MARKDOWN</p>\n');
  });
});
