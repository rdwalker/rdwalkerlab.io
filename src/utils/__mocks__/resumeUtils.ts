const getResumeText = jest.fn().mockResolvedValue(
  '<section id="g1-section"><h2 id="g1">Resume Markdown HTML Mock</h2></section>',
);

export default getResumeText;
