import { marked } from 'marked';

import resume from '../assets/resume.md';

const getResumeText = async (): Promise<string> => {
  const response = await fetch(resume as RequestInfo);

  const text = await response.text();

  return marked.parse(text);
};

export default getResumeText;
