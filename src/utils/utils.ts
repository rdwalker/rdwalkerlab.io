const getRandomValueFromArray = (array: string[]): string => (
  array[Math.floor(Math.random() * array.length)]
);

export default getRandomValueFromArray;
