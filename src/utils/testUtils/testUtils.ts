import mediaQuery from 'css-mediaquery';

/**
 * Creates a media match for unit testing responsiveness.
 */
function createMatchMedia(width: number) {
  return (query: string): MediaQueryList => ({
    matches: mediaQuery.match(query, { width }),
    media: 'test',
    onchange: null,
    dispatchEvent: (): boolean => true,
    addListener: (): void => undefined,
    removeListener: (): void => undefined,
    addEventListener: (): void => undefined,
    removeEventListener: (): void => undefined,
  });
}

export default createMatchMedia;
