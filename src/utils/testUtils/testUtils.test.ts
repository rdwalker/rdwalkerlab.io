import createMatchMedia from './testUtils';

describe('testUtils', () => {
  describe('createMatchMedia()', () => {
    it('should build a MediaQueryList and execute callbacks', () => {
      const matchMedia = createMatchMedia(1000)('test');
      expect(matchMedia.addListener(null)).toBeUndefined();
      expect(matchMedia.removeListener(null)).toBeUndefined();
      expect(matchMedia.dispatchEvent(new Event('test'))).toBe(true);
      expect(matchMedia.addEventListener('test', () => undefined)).toBeUndefined();
      expect(matchMedia.removeEventListener('test', () => undefined)).toBeUndefined();
    });
  });
});
