import getRandomValueFromArray from './utils';

describe('utils', () => {
  describe('getRandomArrayValue()', () => {
    it('should return a random value from the array', () => {
      const array = ['A', 'B', 'C'];
      const value = getRandomValueFromArray(array);

      expect(array).toContain(value);
    });
  });
});
