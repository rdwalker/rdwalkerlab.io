import React from 'react';
import { AppBar, Box, Button, Hidden, IconButton, Toolbar, Tooltip, useTheme } from '@mui/material';
import { Brightness7, Brightness4, Menu } from '@mui/icons-material';
import { Link as RouterLink, useLocation } from 'react-router-dom';

interface Props {
  toggleTheme: () => void;
  toggleResumeMenu: () => void;
}

function AppHeader(
  {
    toggleTheme,
    toggleResumeMenu,
  }: Props,
): React.ReactElement {
  const theme = useTheme();
  const location = useLocation();

  return (
    <AppBar
      position="sticky"
      color={theme.palette.mode === 'dark' ? 'primary' : 'secondary'}
      sx={{
        zIndex: theme.zIndex.drawer + 1,
      }}
    >
      <Toolbar
        sx={{
          '& button, a': {
            mx: 1,
          },
        }}
        disableGutters
      >
        {
          location.pathname.includes('/resume')
          && (
            <Hidden mdUp>
              <Tooltip title="Table of Contents">
                <IconButton color="inherit" onClick={toggleResumeMenu} size="large">
                  <Menu />
                </IconButton>
              </Tooltip>
            </Hidden>
          )
        }
        <Box flexGrow={1} />
        <Button color="inherit" component={RouterLink} to="/">
          Home
        </Button>
        <Button color="inherit" component={RouterLink} to="/resume">
          Resume
        </Button>
        <Button
          component="a"
          variant="outlined"
          color="inherit"
          href="mailto: dylan_walker@yahoo.com"
        >
          Contact
        </Button>
        <Tooltip title="Toggle Light/Dark Theme">
          <IconButton color="inherit" onClick={toggleTheme} size="large">
            {theme.palette.mode === 'dark' ? <Brightness7 /> : <Brightness4 />}
          </IconButton>
        </Tooltip>
      </Toolbar>
    </AppBar>
  );
}

export default AppHeader;
