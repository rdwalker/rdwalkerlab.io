import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter } from 'react-router-dom';

import createMatchMedia from 'src/utils/testUtils/testUtils';
import AppHeader from './AppHeader';

describe('<AppHeader />', () => {
  const toggleThemeMock = jest.fn();
  const toggleResumeMock = jest.fn();

  const setup = (location: string, width: number) => {
    window.matchMedia = createMatchMedia(width);

    return (
      <MemoryRouter initialEntries={[location]} initialIndex={0}>
        <AppHeader toggleTheme={toggleThemeMock} toggleResumeMenu={toggleResumeMock} />
      </MemoryRouter>
    );
  };

  it('should render without error', () => {
    render(setup('/', 1000));

    expect(screen.getByRole('link', { name: 'Home' })).not.toBeNull();
  });

  it('should toggle theme', () => {
    render(setup('/', 1000));

    userEvent.click(screen.getByRole('button', { name: 'Toggle Light/Dark Theme' }));

    expect(toggleThemeMock).toHaveBeenCalledTimes(1);
  });

  it('should toggle menu if at resume and small', () => {
    render(setup('/resume', 100));

    userEvent.click(screen.getByRole('button', { name: 'Table of Contents' }));

    expect(toggleResumeMock).toHaveBeenCalledTimes(1);
  });
});
