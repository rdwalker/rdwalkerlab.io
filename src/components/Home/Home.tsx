import React, { useEffect, useRef, useState } from 'react';
import { Box, Button, Fade, Grid, Typography, useTheme } from '@mui/material';
import { Link } from 'react-router-dom';

import lightBackground from 'src/assets/lightBackground.jpg';
import darkBackground from 'src/assets/darkBackground.jpg';
import { adjectives, nouns } from 'src/descriptions';
import useInterval from '../../hooks/useInterval';
import getRandomValueFromArray from '../../utils/utils';


function Home(): React.ReactElement {
  const theme = useTheme();
  const adjective = useRef(getRandomValueFromArray(adjectives));
  const noun = useRef(getRandomValueFromArray(nouns));

  const [description, setDescription] = useState<{ description: string; fade: boolean }>({
    description: `${adjective.current} ${noun.current}`,
    fade: true,
  });

  useInterval(() => {
    while (`${adjective.current} ${noun.current}` === description.description) {
      adjective.current = getRandomValueFromArray(adjectives);
      noun.current = getRandomValueFromArray(nouns);
    }

    setDescription({
      description: `${adjective.current} ${noun.current}`,
      fade: true,
    });
  }, 10000);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDescription((prev) => ({ ...prev, fade: false }));
    }, 9000);

    return (): void => {
      window.clearTimeout(timeout);
    };
  }, [description]);

  return (
    <Box
      display="flex" sx={{
        display: 'flex',
        flexGrow: 1,
        p: 1,
        '&::after': {
          content: '" "',
          display: 'block',
          position: 'absolute',
          left: 0,
          top: 0,
          width: '100%',
          height: '100%',
          backgroundImage: `url(${theme.palette.mode === 'dark' ? darkBackground : lightBackground})`,
          backgroundSize: 'cover',
          opacity: 0.4,
        },
      }}
      data-testid="background"
    >
      <Grid container justifyContent="center" alignContent="center" spacing={2} sx={{ zIndex: 1 }}>
        <Grid item xs={12}>
          <Typography color="inherit" align="center" variant="h1">
            Dylan Walker
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Fade in={description.fade} timeout={1000}>
            <Typography color="inherit" align="center" variant="h2" data-testid="description">
              {description.description}
            </Typography>
          </Fade>
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            color="inherit"
            component={Link}
            to="/resume"
            size="large"
          >
            Resume
          </Button>
        </Grid>
        <Grid item>
          <Button
            component="a"
            variant="outlined"
            size="large"
            color="inherit"
            href="mailto: dylan_walker@yahoo.com"
          >
            Contact
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Home;
