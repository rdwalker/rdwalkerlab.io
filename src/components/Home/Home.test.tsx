import React from 'react';
import { render, act, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material';
import getRandomValueFromArray from 'src/utils/utils';

import Home from './Home';

jest.mock('src/utils/utils');

describe('<Home />', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.clearAllTimers();
  });

  const setup = () => (
    <MemoryRouter>
      <Home />
    </MemoryRouter>
  );

  it('should render without error', () => {
    render(setup());

    expect(screen.getByText('Dylan Walker')).not.toBeNull();
  });

  it('should update the after 10 seconds', () => {
    render(setup());

    const description = screen.getByTestId('description');
    const description1 = description.innerHTML;

    act(() => {
      (getRandomValueFromArray as jest.Mock).mockReturnValueOnce('test');
      (getRandomValueFromArray as jest.Mock).mockReturnValueOnce('test');
      (getRandomValueFromArray as jest.Mock).mockReturnValueOnce('test2');
      jest.advanceTimersByTime(10001);
    });

    const description2 = description.innerHTML;

    expect(description1).not.toEqual(description2);
  });
});
