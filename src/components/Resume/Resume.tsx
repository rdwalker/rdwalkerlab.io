import React, { useEffect, useState } from 'react';
import { Box, Drawer, Hidden, LinearProgress, Toolbar, Typography, useTheme } from '@mui/material';

import getResumeText from 'src/utils/resumeUtils';
import PCancelable from 'p-cancelable';
import TableOfContents, { TOCContent } from './TableOfContents';

const getDimensions = (
  ele: HTMLElement | null,
): {
  height: number;
  offsetTop: number;
  offsetBottom: number;
} => {
  if (ele) {
    const { height } = ele.getBoundingClientRect();
    const { offsetTop } = ele;
    const offsetBottom = offsetTop + height;

    return {
      height,
      offsetTop: offsetTop - 20,
      offsetBottom,
    };
  }
  return {
    height: 0,
    offsetTop: 0,
    offsetBottom: 0,
  };
};

export interface Props {
  showMenu: boolean;
  toggleMenu: () => void;
}

function Resume({ showMenu, toggleMenu }: Props): React.ReactElement {
  const theme = useTheme();

  const [resumeHTML, setResumeHTML] = useState<string>();
  const [menuHeadings, setMenuHeadings] = useState<TOCContent | null>(null);
  const [selectedHeading, setSelectedHeading] = useState<string>();
  const [error, setError] = useState('');

  const onHeadingMenuClick = (id: string) => (): void => {
    setSelectedHeading(id);
  };

  useEffect(() => {
    const getResume = new PCancelable<string>((resolve, _, onCancel) => {
      getResumeText().then((html) => {
        resolve(html);
      }).catch(() => {
        setError('Error retrieving resume. Please try again.');
      });

      /* eslint no-param-reassign: ["error", { "props": false }] */
      onCancel.shouldReject = false;
    });

    getResume.then((result) => {
      const headingRegex = /<h2 id="(.*?)">(.*?|[\s\S]*?)<\/h2>/gi;

      const matches = result.matchAll(headingRegex);
      const toc: TOCContent = [];
      Array.from(matches).forEach((match) => {
        toc.push({
          id: `${match[1]}-section`,
          text: match[2],
        });
      });

      setMenuHeadings(toc);
      setResumeHTML(result);
    }).catch(() => {
      setError('Error parsing resume. Please try again.');
    });

    return ((): void => {
      getResume.cancel();
    });
  }, []);

  useEffect(() => {
    const scrollItemIds = menuHeadings ? menuHeadings.map((heading) => heading.id) : [];
    const contentBox: HTMLElement = (document.getElementById('resumeContent') as HTMLElement);

    const handleScroll = (): void => {
      const headerEl = document.getElementById('appHeader');

      const { height: headerHeight } = getDimensions(headerEl);
      const scrollPosition = contentBox.scrollTop + headerHeight;

      const inView = scrollItemIds.find((id) => {
        const menuEl = document.getElementById(id);

        const { offsetBottom, offsetTop } = getDimensions(menuEl);
        return scrollPosition > offsetTop && scrollPosition < offsetBottom;
      });

      setSelectedHeading(inView);
    };

    handleScroll();

    contentBox.addEventListener('scroll', handleScroll);

    return (): void => {
      contentBox.removeEventListener('scroll', handleScroll);
    };
  }, [menuHeadings]);

  return (
    <Box
      sx={{
        ml: 3,
        pr: 3,
        py: 3,
        [theme.breakpoints.up('md')]: {
          width: `calc(100% - ${theme.spacing(36)})`,
        },
        overflow: 'auto',
      }}
      id="resumeContent"
      role="article"
    >
      {error || (
        resumeHTML
          ? (
            <>
              <Hidden mdUp>
                <Drawer
                  open={showMenu}
                  variant="temporary"
                  anchor="left"
                  onClose={toggleMenu}
                  keepMounted
                  disablePortal
                >
                  <TableOfContents
                    menuHeadings={menuHeadings}
                    selectedHeading={selectedHeading}
                    onHeadingMenuClick={onHeadingMenuClick}
                  />
                </Drawer>
              </Hidden>
              <Typography component="div" color="inherit">
                {/* Intentional */}
                {/* eslint-disable-next-line react/no-danger */}
                <div dangerouslySetInnerHTML={{ __html: resumeHTML }} />
              </Typography>
              <Hidden mdDown>
                <Drawer variant="permanent" anchor="right">
                  <Toolbar />
                  <TableOfContents
                    menuHeadings={menuHeadings}
                    selectedHeading={selectedHeading}
                    onHeadingMenuClick={onHeadingMenuClick}
                  />
                </Drawer>
              </Hidden>
              <Box component={'div'} sx={{ height: '80vh' }} />
            </>
          )
          : <LinearProgress />
      )}
    </Box>
  );
}

export default Resume;
