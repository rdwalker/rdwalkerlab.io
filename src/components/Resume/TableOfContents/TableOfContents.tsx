import React from 'react';
import { List, ListItem, Typography, useTheme } from '@mui/material';

export type TOCContent = {
  id: string;
  text: string;
}[];

interface Props {
  menuHeadings: TOCContent | null;
  selectedHeading: string | undefined;
  onHeadingMenuClick: (id: string) => () => void;
}

function TableOfContents(
  {
    menuHeadings,
    selectedHeading,
    onHeadingMenuClick,
  }: Props,
): React.ReactElement {
  const theme = useTheme();

  return (
    <List sx={{
      width: theme.spacing(30),
      paddingTop: 0,
    }}>
      {menuHeadings && (
        menuHeadings.map((heading) => (
          <ListItem
            key={heading.id}
            button
            component="a"
            href={`#${heading.id}`}
            sx={selectedHeading === heading.id ? {
              borderLeft: `${theme.spacing(0.5)} solid ${theme.palette.mode === 'dark' ? '#FFFFFF' : '#424242'}`,
            } : {
              borderLeft: `${theme.spacing(0.5)} solid transparent`,
            }}
            onClick={onHeadingMenuClick(heading.id)}
          >
            <Typography sx={{ textTransform: 'capitalize' }}>
              {heading.text.toLowerCase()}
            </Typography>
          </ListItem>
        ))
      )}
    </List>
  );
}

export default TableOfContents;
