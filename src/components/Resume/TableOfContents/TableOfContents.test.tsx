import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import TableOfContents from './TableOfContents';

describe('<TableOfContents />', () => {
  const menuClickMock = jest.fn();

  it('should render without error', () => {
    render(
      <TableOfContents
        menuHeadings={null}
        selectedHeading={undefined}
        onHeadingMenuClick={menuClickMock}
      />,
    );

    expect(screen.getByRole('list')).not.toBeNull();
  });

  it('should render with menu headings and click them', () => {
    render(
      <TableOfContents
        menuHeadings={[{ id: 'test', text: 'Test' }]}
        selectedHeading={undefined}
        onHeadingMenuClick={menuClickMock}
      />,
    );

    userEvent.click(screen.getByRole('link', { name: 'test' }));

    expect(menuClickMock).toHaveBeenCalledTimes(1);
    expect(menuClickMock).toHaveBeenCalledWith('test');
  });

  it('should render with menu heading selected', () => {
    render(
      <TableOfContents
        menuHeadings={[{ id: 'test', text: 'Test' }]}
        selectedHeading="test"
        onHeadingMenuClick={menuClickMock}
      />,
    );

    expect(window.getComputedStyle(screen.getByRole('link')).borderLeftColor).not.toBe('transparent');
  });

  it('should render without menu heading selected', () => {
    render(
      <TableOfContents
        menuHeadings={[{ id: 'test', text: 'Test' }]}
        selectedHeading={undefined}
        onHeadingMenuClick={menuClickMock}
      />,
    );

    expect(window.getComputedStyle(screen.getByRole('link')).borderLeftColor).toBe('transparent');
  });
});
