import React from 'react';
import { Drawer, Hidden } from '@mui/material';
import { Props } from '../Resume';

function Resume({ showMenu, toggleMenu }: Props): React.ReactElement {
  return <>
    Resume Mock
    <Hidden mdUp>
      <Drawer open={showMenu} onClose={toggleMenu} variant="temporary">
        TOC Mock
      </Drawer>
    </Hidden>
    <Hidden mdDown>
      TOC Mock
    </Hidden>
  </>;
}

export default Resume;
