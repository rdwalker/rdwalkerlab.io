import React from 'react';
import {
  render,
  fireEvent,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter } from 'react-router-dom';

import Resume from './Resume';
import getResumeText from '../../utils/resumeUtils';
import createMatchMedia from '../../utils/testUtils/testUtils';

jest.mock('../../utils/resumeUtils');

describe('<Resume />', () => {
  const toggleMenuMock = jest.fn();

  beforeAll(() => {
    window.matchMedia = createMatchMedia(1000);
  });

  const setup = () => (
    <MemoryRouter>
      <Resume showMenu={false} toggleMenu={toggleMenuMock} />
    </MemoryRouter>
  );

  it('should render without error', async () => {
    render(setup());

    expect(await screen.findByText('Resume Markdown HTML Mock')).not.toBeNull();
  });

  it('should click on TOC items and set them as selected', async () => {
    render(setup());
    await screen.findByText('Resume Markdown HTML Mock');

    userEvent.click(screen.getByRole('link', { name: 'resume markdown html mock' }));

    const tocSection = screen.getByRole('link');

    expect(window.getComputedStyle(tocSection).borderLeftColor).not.toBe('transparent');
  });

  it('should set selected element when window is scrolled to it', async () => {
    Element.prototype.getBoundingClientRect = (): DOMRect => (
      {
        x: 0,
        y: 0,
        height: 200,
        width: 0,
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        toJSON: jest.fn(),
      }
    );
    render(setup());
    await screen.findByText('Resume Markdown HTML Mock');
    const content = screen.getByRole('article');

    fireEvent.scroll(content, { target: { scrollTop: 50 } });

    const tocSection = screen.getByRole('link');

    expect(window.getComputedStyle(tocSection).borderLeftColor).not.toBe('transparent');
  });

  it('should show error if there is an error parsing the resume', async () => {
    (getResumeText as jest.Mock).mockRejectedValue(new Error('fail'));
    render(setup());

    expect(await screen.findByText(/Error/)).not.toBeNull();
  });

  it('should show error if there is an error retrieving the resume', async () => {
    // Resolve a number so `matchAll` fails, which is probably the only place the parse can fail.
    (getResumeText as jest.Mock).mockResolvedValue(1);
    render(setup());

    expect(await screen.findByText(/Error/)).not.toBeNull();
  });
});
