import React from 'react';

interface State {
  hasError: boolean;
}

class ErrorBoundary extends
  React.Component<React.PropsWithChildren<Record<string, unknown>>, State> {
  constructor(props: Record<string, unknown>) {
    super(props);
    this.state = { hasError: false };
  }

  private static getDerivedStateFromError(): { hasError: boolean } {
    return { hasError: true };
  }

  render(): React.ReactNode {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return (
        <h1>Oops! Something went wrong.  Please refresh the page.</h1>
      );
    }
    return children;
  }
}
export default ErrorBoundary;
