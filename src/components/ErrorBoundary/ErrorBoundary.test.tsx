import React from 'react';
import { render, screen } from '@testing-library/react';

import ErrorBoundary from './ErrorBoundary';

describe('<ErrorBoundary />', () => {
  it('should error if component errors during render and allow for page refresh', () => {
    /* eslint-disable no-console */
    const oldError = console.error;
    console.error = jest.fn();

    const BuggyComponent = () => {
      throw new Error('Crash!');
    };

    render(<ErrorBoundary><BuggyComponent /></ErrorBoundary>);

    expect(screen.getByText(/Oops!.*/gi)).not.toBeNull();

    console.error = oldError;
    /* eslint-enable no-console */
  });
});
