import { useEffect, useRef } from 'react';

type AnyFunction = (...args: never[]) => void;

function useInterval(callback: AnyFunction, delay: number | null): void {
  const savedCallback = useRef<AnyFunction>(callback);

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick(): void {
      savedCallback.current();
    }

    if (delay !== null) {
      const id = setInterval(tick, delay);
      return (): void => { clearInterval(id); };
    }

    return undefined;
  }, [delay]);
}

export default useInterval;
