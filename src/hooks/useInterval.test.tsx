import React from 'react';
import { render } from '@testing-library/react';
import useInterval from './useInterval';

const mockFn = jest.fn();

function TestComponent(
  { delay }: { delay: number | null },
): React.ReactElement {
  useInterval(mockFn, delay);

  return <div />;
}

describe('useInterval', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    mockFn.mockClear();
    jest.clearAllTimers();
  });

  it('should execute a callback after time delay', () => {
    render(<TestComponent delay={500} />);

    jest.runOnlyPendingTimers();
    expect(mockFn).toHaveBeenCalledTimes(1);
  });

  it('should not execute a callback if delay is null', () => {
    render(<TestComponent delay={null} />);

    jest.runOnlyPendingTimers();
    expect(mockFn).not.toHaveBeenCalled();
  });
});
