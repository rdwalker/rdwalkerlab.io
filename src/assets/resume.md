# R. DYLAN WALKER

I am a hard-working, dedicated, creative engineer looking to work as a team to solve complex, real-world problems.
Extremely quick learner that commits to solving a problem and getting it done to project specifications on-time.

<section id="professional-highlights-section" role="region">

## PROFESSIONAL HIGHLIGHTS
* Lead a team of software engineers through release of two products with multiple versions.
* Creative, self-taught programmer very capable of learning new skills to meet project demands.
* Ten (10) years of experience developing programs to automate workflows, quality control checks, and modeling in support of engineering and scientific investigations.
* Six (6) years of experience designing and managing MongoDB, SQL Server, and Microsoft Access databases for scientific and engineering disciplines; including web development.
* Very experienced in organizing, cleaning, manipulating, and transferring data using Python, JavaScript, and SQL.
</section>

<section id="proficiencies-section" role="region">

## PROFICIENCIES

<table class="skills">
<tr>
<td nowrap>&#9679; <strong>ReactJS and Angular</strong></td>
<td nowrap>&#9679; <strong>JavaScript and Typescript</strong></td>
<td nowrap>&#9679; <strong>Jest</strong></td>
<td nowrap>&#9679; <strong>Cypress</strong></td>
<td>&#9679; Docker</td>
</tr>
<tr>
<td>&#9679; Python</td>
<td>&#9679; HTML</td>
<td>&#9679; CSS</td>
<td>&#9679; Zeek</td>
<td>&#9679; JetBrains IDEs (WebStorm, etc.)</td>
</tr>
<tr>
<td>&#9679; Git</td>
<td>&#9679; Markdown</td>
<td>&#9679; Linux</td>
<td></td>
<td></td>
</tr>
</table>
</section>

<section id="government-clearance-section" role="region">

## GOVERNMENT CLEARANCE
* Top Secret

</section>

<section id="professional-certifications-section" role="region">

## PROFESSIONAL CERTIFICATIONS
* Certified Ethnical Hacker (October 2020)
* Security+ (October 2018)
* Recipient of the Grow with Google/Udacity Web Development Scholarship

</section>

<section id="professional-experience-section" role="region">

## PROFESSIONAL EXPERIENCE

#### April 2018 - Present

**Senior Software Engineer**, Parsons
* Responsible for the development of a web applications using React for efficient data collection and retrieval, cyberspace warfare, and network simulation
* Effectively communicated product development through regularly hosted and chaired meetings with senior stakeholders
* Setup and maintained CI/CD pipelines
* Regularly helped other staff with technical issues related to web development
* Zeek script development

#### 2012 - April 2018

**Programmer/Analyst**, Geosyntec Consultants, Inc. 
* Automated several work functions to improve efficiency via Python and VBA
* Utilized Python to build analysis tools, complete data mining and data-monitoring activities (i.e., bots).
* Produced tools and applications for end-client use to automate engineering analyses, investigations, and quality control checks
* Produced front-end, web-based data visualizations and applications, including spatial data collection and analysis tools
* Integrated existing applications with new tools or applications to create cohesive and complete workflows through custom code in Python, JavaScript, VBA, and SQL
* Trained users on proper use of developed utilities
</section>
<section id="interests-and-hobbies-section" role="region">

## INTERESTS AND HOBBIES
* Built all of my own computers, including a network-attached storage (NAS) server running FreeBSD
* Setup home-network to support 802.11ac and several ethernet connections
* Complete a light-show set to music each year during the holidays utilizing open-source software, a Raspberry Pi, and controllers I solder together myself
* Ultra-marathon running.
</section>
