import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import createMatchMedia from 'src/utils/testUtils/testUtils';
import App from './App';

jest.mock('./utils/resumeUtils');
jest.mock('./components/Home/Home');
jest.mock('./components/Resume/Resume');

describe('<App />', () => {
  it('should render without error', () => {
    render(<App />);

    expect(screen.getByRole('link', { name: 'Home' })).not.toBeNull();
  });

  it('should switch to dark mode and save in localStorage', () => {
    render(<App />);

    const appHeader = screen.getByRole('banner');
    if (!appHeader) {
      throw new Error('Could not find appHeader');
    }

    expect(appHeader.className).toContain('colorSecondary');

    userEvent.click(screen.getByRole('button', { name: 'Toggle Light/Dark Theme' }));

    expect(appHeader.className).toContain('colorPrimary');
    expect(localStorage.getItem('dwalker_theme')).toBe('true');
  });

  it('should navigate to the Resume', async () => {
    render(<App />);

    userEvent.click(screen.getByRole('link', { name: 'Resume' }));

    expect(await screen.findByText('Resume Mock')).not.toBeNull();
  });

  it('should show menu when toggled', async () => {
    window.matchMedia = createMatchMedia(100);
    render(<App />);
    userEvent.click(screen.getByRole('link', { name: 'Resume' }));

    await screen.findByText('Resume Mock');
    userEvent.click(screen.getByRole('button', { name: 'Table of Contents' }));

    expect(screen.getByText('TOC Mock')).not.toBeNull();
  });
});
