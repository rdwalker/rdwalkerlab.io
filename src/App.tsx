import { useState, Suspense, useEffect, useMemo, lazy, ReactElement } from 'react';
import {
  Box,
  createTheme,
  LinearProgress,
  Paper,
  ThemeProvider,
  useMediaQuery,
} from '@mui/material';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

import ErrorBoundary from './components/ErrorBoundary';
import AppHeader from './components/AppBar';

const Home = lazy(() => import('./components/Home'));
const Resume = lazy(() => import('./components/Resume'));

function App(): ReactElement {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const [darkTheme, setDarkTheme] = useState(
    localStorage.getItem('dwalker_theme') === 'true' || prefersDarkMode,
  );

  const [resumeMenu, setResumeMenu] = useState(false);

  const toggleTheme = (): void => {
    setDarkTheme((prev) => !prev);
  };

  const toggleResumeMenu = (): void => {
    setResumeMenu((prev) => !prev);
  };

  useEffect(() => {
    localStorage.setItem('dwalker_theme', darkTheme.toString());
  }, [darkTheme]);

  const theme = useMemo(
    () => createTheme({
      palette: {
        primary: {
          main: '#2c387e',
        },
        secondary: {
          main: '#00b0ff',
        },
        mode: darkTheme ? 'dark' : 'light',
      },
      typography: {
        fontFamily: 'Consolas',
      },
    }),
    [darkTheme],
  );

  return (
    <ErrorBoundary>
      <Router>
        <ThemeProvider theme={theme}>
          <Paper
            square
            elevation={0}
            sx={{
              height: '100%',
              width: '100%',
              overflow: 'auto',
              display: 'flex',
              flexDirection: 'column',
              flexGrow: 1,
            }}
          >
            <AppHeader
              toggleResumeMenu={toggleResumeMenu}
              toggleTheme={toggleTheme}
            />
            <Box
              height="100%"
              width="100%"
              overflow="auto"
              display="flex"
            >
              <Suspense fallback={<LinearProgress />}>
                <Routes>
                  <Route path="/resume" element={<Resume
                    showMenu={resumeMenu}
                    toggleMenu={toggleResumeMenu}
                  />} />
                  <Route path="/" element={ <Home />} />
                </Routes>
              </Suspense>
            </Box>
          </Paper>
        </ThemeProvider>
      </Router>
    </ErrorBoundary>
  );
}

export default App;
