export const adjectives = [
  'Passionate',
  'Creative',
  'Engaged',
  'Enthusiastic',
  'Experienced',
  'JavaScript',
  'TypeScript',
  'React',
  'Frontend',
];

export const nouns = [
  'Developer',
  'Engineer',
  'Programmer',
];
